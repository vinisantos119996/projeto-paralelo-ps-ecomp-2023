# projeto-paralelo-ps-ecomp-2023

> Elen Pasqualli Gesser

> João Guilherme Almeida Couto

> Vinicius Alves dos Santos

### Descrição

Neste projeto, utilizamos os conhecimentos adquiridos nos semestres anteriores e nas capacitações disponibilizadas pela Ecomp para recriar um modelo de site, que nos foi enviado, e deixá-lo responsivo para vários tamanhos de tela.

### Desenvolvimento

O projeto foi feito, em conjunto, por todos os integrantes do grupo, onde nos reunimos remota e presencialmente para podermos nos dedicar na construção do site e todos participaram de todas as etapas, desde procurar referência até escrever linhas de código.


Tivemos várias dificuldades pelo caminho onde, uma delas, foi o tempo. Ao dar início após a semana de provas, sobou pouco tempo para realizar o projeto mas, fizemos o nosso melhor, com o conhecimento e tempo disponíveis. Outra dificuldade enfrentada foi o sistema para responsividade do Bootstrap, do qual não tinhamos conhecimento suficiente para que pudessemos deixar o site responsivo da melhor forma, assim tendo problemas quando trocavamos de dispositivo ou máquina para dar continuidade ao projeto.

### Considerações finais

Por fim, não conseguimos deixar o layout da forma desejada, mas não deixamos de nos esforçar, buscar ajuda, buscar referência e de dar o nosso melhor em nenhum momento, mesmo com tais dificuldades, a persistência e resiliência imperou em todos os passos do projeto.
